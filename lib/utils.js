const replaceAt = (str,index,chr) => {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);}

const splitWord = (str) =>{
          return str.split(" ");
        }
        
const joinWord = (str) =>{
          return str.join(" ");
        } 

const word = (str)=>{
    word_one=splitWord(str)
    final_word=joinWord(word_one)
    return final_word;
    }


module.exports = {
   replaceAt: replaceAt,
   splitWord: splitWord,
   joinWord: joinWord,
   word: word
}